# Ejemplos de uso de etiquetas de HTML

> **Versión:** v1.0.0

En este repositorio se encuentra construido como un proyecto incremental en donde partiendo desde lo más elemental de HTML se van agregando elementos a la construcción de un sitio sencillo.

## Instrucciones de uso

Cada uno de los ejemplos se fueron generando como tags en el repositorio, por lo que para acceder cualquiera de ellos bastará con clonar o bien hacer un checkout al tag relacionado.

~~~bash
#!/bin/bash

# Clonar el repositorio
git clone -b $taget_tag https://gitlab.com/pragma-garage/frontend/html-and-css/html-samples.git

# Cambiar al tag deseado
git checkout $target_tag
~~~

> A partir del tag `v0.4.0` será necesario que el proyecto se abra en el editor VSCode y se ejecute mediante la extensión [Live Server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer).
>
> La configuración que se encuentra en el archivo `.vscode/settings.json` apunta a que cuando se inicie el servidor de prueba, se ejecute en [localhost:3000](http://localhost:3000/), sirviendo el contenido desde la carpeta `public`.

## Listado de TAGS

- v0.1.0: Estructura mínima de un documento HTML
- v0.2.0: Estructura básica del texto
- v0.3.0: Estructuración de listas
- v0.4.0: Hipervínculos en HTML
- v0.5.0: Formularios
- v0.6.0: Meta etiquetas
- v0.7.0: Inclusión de recursos (imágenes, audios, videos y contenido incrustado)
- v1.0.0: Semántica
